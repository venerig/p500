# p500
![robotframework](https://img.shields.io/badge/powered%20by-robotframework-brightgreen)

This is the repository for P500 bot project.
This project is a web bot for site plus500.

## Usage

To run, you can choose to `git clone`. 

## Test sections

Here the project structure:
```
robotframework/
    /atest
        /config
            /browser.json
        /output
            /pabot_results
        /src
            /kws
                /kws_form1.resource
            /page_objects
                /form1.resource
            /snippets
                /snippet.resource
            /utils
                /AsyncResponse.py
                /capabilities.py
                /custom_kw.py
            /vars
                /locators.yml
                /main.resource
            /common.resource
        /tests
            /e2e
                /e2e.robot
            /form1
                /form_validation.robot
                /test_form1.robot
        /video

    /README
    /.gitignore
    /docker-compose.yml
```
<!-- NB: A dummy file is left on the tests/ root to prevent testing all files on commit. -->