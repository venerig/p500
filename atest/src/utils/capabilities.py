from selenium import webdriver
from robot.errors import RobotError
import os


def get_browser_options(browser, headless=False):
    if browser.lower() == "chrome":
        return get_chrome_options(headless=headless)
    elif browser == "firefox" or "ff":
        return get_ff_options(headless=headless)
    else:
        raise RobotError("unexpected")

def get_chrome_options(headless=False):
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("--disable-popup-blocking")
    chrome_options.add_argument("--disable-extensions")
    chrome_options.add_argument("--disable-infobars")
    chrome_options.add_argument("--ignore-certificate-errors")
    chrome_options.add_argument("--enable-features=NetworkService,NetworkServiceInProcess")
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_argument("--disable-gpu")
    chrome_options.add_argument("--disable-browser-side-navigation")
    chrome_options.add_experimental_option("excludeSwitches", ["enable-automation"])
    if headless:
        chrome_options.add_argument("--window-size=1920,960")
        chrome_options.headless = True
    return chrome_options

def get_ff_options(headless=False):
    os.environ['MOZ_HEADLESS_WIDTH'] = '1920'
    os.environ['MOZ_HEADLESS_HEIGHT'] = '960'
    firefox_options = webdriver.FirefoxOptions()
    firefox_options.add_argument("--no-sandbox")
    firefox_options.add_argument("--disable-gpu")
    firefox_options.add_argument("--disable-extensions")
    if headless:
        firefox_options.headless = True
    return firefox_options

def get_chrome_mobile_options(headless=False, device="iPhone X"):
    chrome_options = webdriver.ChromeOptions()
    mobile_emulation = {"deviceName": device}
    chrome_options.add_experimental_option("mobileEmulation", mobile_emulation)
    chrome_options.add_argument("--disable-popup-blocking")
    chrome_options.add_argument("--disable-extensions")
    chrome_options.add_argument("--disable-infobars")
    chrome_options.add_argument("--ignore-certificate-errors")
    chrome_options.add_argument("--enable-features=NetworkService,NetworkServiceInProcess")
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_argument("--disable-gpu")
    chrome_options.add_argument("--disable-browser-side-navigation")
    chrome_options.add_experimental_option("excludeSwitches", ["enable-automation"])
    if headless:
        chrome_options.headless = True
    return chrome_options
