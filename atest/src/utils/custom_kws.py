from robot.libraries.BuiltIn import BuiltIn
from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver.support.wait import WebDriverWait
from robot.errors import RobotError
from dateutil.relativedelta import relativedelta
from robot.api.deco import keyword
import datetime


def wait_ultil_element_contains_class(locator, clazz, timeout=5):
    timeout = int(timeout)
    sl = BuiltIn().get_library_instance('SeleniumLibrary')
    try:
        WebDriverWait(sl.driver, timeout).until(wait_for_element_contains_class(locator, clazz))
    except:
        if isinstance(locator, str):
            raise(RobotError("Element: {} does not contain class {}".format(locator, clazz)))
        else:
            raise(RobotError("Element does not contain class {}".format(clazz)))

def wait_ultil_element_does_not_contain_class(locator, clazz, timeout=5):
    timeout = int(timeout)
    sl = BuiltIn().get_library_instance('SeleniumLibrary')
    try:
        WebDriverWait(sl.driver, timeout).until(wait_for_element_contains_class(locator, clazz, not_contains=True))
    except:
        if isinstance(locator, str):
            raise(RobotError("Element: {} contains class {}".format(locator, clazz)))
        else:
            raise(RobotError("Element contains class {}".format(clazz)))

def wait_ultil_element_or_parents_contain_class(locator, clazz, timeout=5):
    timeout = int(timeout)
    sl = BuiltIn().get_library_instance('SeleniumLibrary')
    try:
        WebDriverWait(sl.driver, timeout).until(wait_for_element_or_parent_contains_class(locator, clazz))
    except:
        if isinstance(locator, str):
            raise(RobotError("Element: {}  or its parents do not contain class {}".format(locator, clazz)))
        else:
            raise(RobotError("Element or its parents do not contain class {}".format(clazz)))

def wait_ultil_element_or_parents_do_not_contain_class(locator, clazz, timeout=5):
    timeout = int(timeout)
    sl = BuiltIn().get_library_instance('SeleniumLibrary')
    try:
        WebDriverWait(sl.driver, timeout).until(wait_for_element_or_parent_contains_class(locator, clazz, not_contains=True))
    except:
        if isinstance(locator, str):
            raise(RobotError("Element: {}  or its parents do not contain class {}".format(locator, clazz)))
        else:
            raise(RobotError("Element or its parents do not contain class {}".format(clazz)))

@keyword("Get Current Date Decremented By Time")
def get_current_date_decremented_by_time(years=0, months=0, days=0):
    date = datetime.datetime.now() - relativedelta(years=years, months=months, days=days)
    return  date.strftime('%Y'), date.strftime('%m'), date.strftime('%d')

class wait_for_element_contains_class(object):
    def __init__(self, locator, clazz, not_contains=False):
        self.locator = locator
        self.clazz = clazz
        self.not_contains = not_contains

    def __call__(self, driver):
        sl = BuiltIn().get_library_instance('SeleniumLibrary')
        element = sl.get_webelement(self.locator)
        try:
            if self.not_contains:
                contains_class = self.clazz not in element.get_attribute("class").lower()
            else:
                contains_class = self.clazz in element.get_attribute("class").lower()
            return contains_class
        except Exception:
            return False


class wait_for_element_or_parent_contains_class(object):
    def __init__(self, locator, clazz, not_contains=False):
        self.locator = locator
        self.clazz = clazz
        self.not_contains = not_contains

    def __call__(self, driver):
        sl = BuiltIn().get_library_instance('SeleniumLibrary')
        script_1 = 'return $(arguments[0]).parents(".{0}").length > 0 || arguments[0].classList.contains("{0}");'.format(self.clazz)
        script_2 = 'return $(arguments[0]).parents(".{0}").length == 0 && !arguments[0].classList.contains("{0}");'.format(self.clazz)
        element = sl.get_webelement(self.locator)
        try:
            if self.not_contains:
                contains_class = sl.driver.execute_script(script_2, element)
            else:
                contains_class = sl.driver.execute_script(script_1, element)
            return contains_class
        except Exception:
            return False
