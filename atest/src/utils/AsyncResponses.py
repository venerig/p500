import httpx
import asyncio
import concurrent

from concurrent.futures.thread import ThreadPoolExecutor
from robot.api.deco import keyword
from robot.libraries.BuiltIn import BuiltIn


async def _async_request(method:str, **kwargs):
    timeout = httpx.Timeout(30.0)
    headers = {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36'
    }
    async with httpx.AsyncClient(verify=False, headers=headers) as client:
        response = None
        if method.upper() == "GET":
            response = await client.get(timeout=timeout, **kwargs)
        elif method.upper() == "POST":
            response = await client.post(timeout=timeout, **kwargs)
        return response


class AsyncResponses(object):
    ROBOT_LIBRARY_SCOPE = 'GLOBAL'

    def __init__(self):
        self.errors = []
        self.executor = ThreadPoolExecutor(max_workers=6)
        self.processes = []
        self.results = []

    @keyword
    def send_async_get_request(self, **kwargs):
        future = self.executor.submit(self._send_async_request, method="GET", **kwargs)
        future.add_done_callback(self.callback)
        self.processes.append(future)

    @keyword
    def send_async_post_request(self, **kwargs):
        future = self.executor.submit(self._send_async_request, method="POST", **kwargs)
        future.add_done_callback(self.callback)
        self.processes.append(future)

    def callback(self, future):
        self.results.append(future.result())
        self.processes.remove(future)

    def _send_async_request(self, method, **kwargs):
        try:
            response = asyncio.run(_async_request(method, **kwargs))
            return response
        except Exception as e:
            self.errors.append("{} {}".format(type(e), e))
            return None

    @keyword
    def wait_all_requests(self):
        concurrent.futures.wait(self.processes)
        if self.errors:
            BuiltIn().log("\n".join(self.errors), level="ERROR")
        self.errors.clear()
        return self.results


if __name__ == "__main__":
    ar = AsyncResponses()
    for i in range(1):
        ar.send_async_post_request(url="https://www.simplesurance.it/assicurazione-pedelec?partner=61716&utm_medium=aff&utm_source=ipvt&vc=SVQwNDA1QU0yMA==")
    l = ar.wait_all_requests()
    print(l)
