*** Settings ***
Documentation   PLUS500 - BOT
Resource        ${CURDIR}${/}..${/}..${/}src${/}common.resource
Test Setup      I open the Browser
Test Teardown   Teardown with Screenshot
Test Template   Close All Position Templating
Force Tags      e2e

*** Test Cases ***
Test-a   pappalo@gmail.com  pappalo@gmail.com

Test-b   pappalo@gmail.com  pappalo@gmail.com

Test-c   nastrida@gmail.com  nastrida@gmail.com

Test-d   narciso5050@mailinator.com  narciso5050@mailinator.com

*** Keywords ***
Close All Position Templating
  [Arguments]  ${user}  ${pwd}
  Go To Url  ${url_home}
  I Click Login  
  I Compile Login Data  ${user}  ${pwd}
  Close All Trade