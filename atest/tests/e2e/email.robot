*** Settings ***
Documentation   PLUS500 - BOT
Resource        ${CURDIR}${/}..${/}..${/}src${/}common.resource
Test Setup      I open the Browser
Test Teardown   Teardown with Screenshot
Test Template   Email Sender Templating
Force Tags      e2e

*** Test Cases ***
Test-a   pappalo@gmail.com  pappalo@gmail.com

Test-b   pappalo@gmail.com  pappalo@gmail.com

Test-c   nastrida@gmail.com  nastrida@gmail.com

Test-d   narciso5050@mailinator.com  narciso5050@mailinator.com

*** Keywords ***
Email Sender Templating
  [Arguments]  ${user}  ${pwd}
  Go To  ${url_home}
  I Click Login  
  I Compile Login Data  ${user}  ${pwd}  
  trade.Demo Mode
  I Send Email  ${{ $from_date[0] }}  ${{ $from_date[1] }}  ${{ $from_date[2] }}  ${{ $to_date[0] }}  ${{ $to_date[1] }}  ${{ $to_date[2] }}