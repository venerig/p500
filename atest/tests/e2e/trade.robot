*** Settings ***
Documentation   PLUS500 - BOT
Resource        ${CURDIR}${/}..${/}..${/}src${/}common.resource
Test Setup      I open the Browser
Test Teardown   Teardown with Screenshot
Test Template   Manage Position Templating
Force Tags      e2e

*** Test Cases ***
Test-a   pappalo@gmail.com  pappalo@gmail.com  is_reverse=${False}  profit_level_expected=10.00
...  loss_level_expected=7.50  amount_modifier_profit=0.00150  amount_modifier_loss=0.00150

Test-b   pappalo@gmail.com  pappalo@gmail.com  is_reverse=${False}  profit_level_expected=10.00
...  loss_level_expected=7.50  amount_modifier_profit=0.00150  amount_modifier_loss=0.00150

Test-c   nastrida@gmail.com  nastrida@gmail.com  is_reverse=${False}  profit_level_expected=15.00
...  loss_level_expected=12.50  amount_modifier_profit=0.00150  amount_modifier_loss=0.00150

Test-d   narciso5050@mailinator.com  narciso5050@mailinator.com  is_reverse=${True}  profit_level_expected=5.00
...  loss_level_expected=12.50  amount_modifier_profit=0.00050  amount_modifier_loss=0.00150

*** Keywords ***
Manage Position Templating
  [Arguments]  ${user}  ${pwd}  ${is_reverse}  ${profit_level_expected}  ${loss_level_expected}  ${amount_modifier_profit}  ${amount_modifier_loss}
  Go To Url  ${url_home}
  I Click Login  
  I Compile Login Data  ${user}  ${pwd}
  I Open Position  ${is_reverse}  ${profit_level_expected}  ${loss_level_expected}  ${amount_modifier_profit}  ${amount_modifier_loss}
